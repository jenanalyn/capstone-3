import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [birthday, setBirthday] = useState('');
  const [email, setEmail] = useState('');
  const [emailVerification, setEmailVerification] = useState('');
  const [password, setPassword] = useState('');
  const [passwordVerification, setPasswordVerification] = useState('');
  const [isActive, setIsActive] = useState(false);


  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (
      email !== '' &&
      password !== '' &&
      passwordVerification !== '' &&
      password === passwordVerification
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password, passwordVerification]);

  // Function to simulate user registration
  function registerUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/registration`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        mobileNo: mobileNo,
        birthday: birthday,
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data.success) {
          setFirstName('');
          setLastName('');
          setMobileNo('');
          setBirthday('');
          setEmail('');
          setEmailVerification('');
          setPassword('');
          setPasswordVerification('');

          Swal.fire({
            title: 'Registration successful',
            icon: 'success',
            text: 'Welcome to Zuitt!',
          });

          navigate('/login');
        } else {
          Swal.fire({
            title: 'Registration failed',
            icon: 'error',
            text: data.message,
          });
        }
      })
      .catch((error) => {
        console.error(error);
        Swal.fire({
          title: 'Registration failed',
          icon: 'error',
          text: 'An error occurred during registration.',
        });
      });
  }
  


  return (
    
    user.id !== null
    ? (
      <Navigate to="/products" />
      ) : (
      <>
      <h2>Registration Form</h2>
       <Form onSubmit={(e) => registerUser(e)}>
         <Form.Group controlId="firstName">
           <Form.Label>First Name</Form.Label>
           <Form.Control
             type="text"
             placeholder="Enter your first name"
             value={firstName}
             onChange={(e) => setFirstName(e.target.value)}
             required
           />
         </Form.Group>

         <Form.Group controlId="lastName">
           <Form.Label>Last Name</Form.Label>
           <Form.Control
             type="text"
             placeholder="Enter your last name"
             value={lastName}
             onChange={(e) => setLastName(e.target.value)}
             required
           />
         </Form.Group>

         <Form.Group controlId="mobileNo">
           <Form.Label>Mobile Number</Form.Label>
           <Form.Control
             type="tel"
             placeholder="Enter your mobile number"
             value={mobileNo}
             onChange={(e) => setMobileNo(e.target.value)}
             required
           />
         </Form.Group>

         <Form.Group controlId="birthday">
           <Form.Label>Birthday</Form.Label>
           <Form.Control
             type="date"
             placeholder="Enter your birthday"
             value={birthday}
             onChange={(e) => setBirthday(e.target.value)}
             required
           />
         </Form.Group>

         <Form.Group controlId="email">
           <Form.Label>Email Address</Form.Label>
           <Form.Control
             type="email"
             placeholder="Enter your email"
             value={email}
             onChange={(e) => setEmail(e.target.value)}
             required
           />
         </Form.Group>

         <Form.Group controlId="emailVerification">
           <Form.Label>Email Verification</Form.Label>
           <Form.Control
             type="email"
             placeholder="Re-enter your email"
             value={emailVerification}
             onChange={(e) => setEmailVerification(e.target.value)}
             required
           />
           <Form.Text className="text-muted">
             Please enter your email again to verify.
           </Form.Text>
         </Form.Group>

         <Form.Group controlId="password">
           <Form.Label>Password</Form.Label>
           <Form.Control
             type="password"
             placeholder="Enter your password"
             value={password}
             onChange={(e) => setPassword(e.target.value)}
             required
           />
         </Form.Group>

         <Form.Group controlId="passwordVerification">
           <Form.Label>Email Verification</Form.Label>
           <Form.Control
             type="password"
             placeholder="Re-enter your password"
             value={passwordVerification}
             onChange={(e) => setPasswordVerification(e.target.value)}
             required
           />
           <Form.Text className="text-muted">
             Please enter your password again to verify.
           </Form.Text>
         </Form.Group>

         {isActive ? (
           <Button variant="primary" type="submit" style={{ marginTop: '1rem' }}>
             Register
           </Button>
         ) : (
           <Button variant="danger" type="submit" style={{ marginTop: '1rem' }} disabled>
             Register
           </Button>
         )}
       </Form>
       </>
      )
     
    ); 

}
