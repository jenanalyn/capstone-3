import React, { useEffect, useState } from 'react';
import { Row } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';

export default function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/allActive`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        setProducts(data);
      });
  }, []);

  return (
    <>
      <h1>Products here</h1>
      <Row className="justify-content-center">
        {products.map(product => (
          <ProductCard key={product._id} productProp={product} />
        ))}
      </Row>
    </>
  );
}
