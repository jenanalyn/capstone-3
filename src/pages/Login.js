import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(true);

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.json())
      .then(data => {
        if (data._id !== undefined) {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        } else {
          setUser({
            id: null,
            isAdmin: null
          });
        }
      });
  };

  function authenticate(e) {
    e.preventDefault();
    localStorage.setItem('email', email);

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(res => res.json())
      .then(data => {
        // Check if the response is valid JSON
        if (typeof data === 'object') {
          console.log(data);

          if (data.access !== undefined) {
            localStorage.setItem('token', data.access);

            // Retrieve user details using the access token
            retrieveUserDetails(data.access);

            Swal.fire({
              title: "Login Successful",
              icon: "success",
              text: "Welcome to ShoppeeSpy"
            });
          } else {
            Swal.fire({
              title: "Authentication failed",
              icon: "error",
              text: "Check your log in credentials and try again!"
            });
          }
        } else {
          // Handle the error when the response is not valid JSON
          console.error("Invalid JSON response:", data);
          // Display an error message to the user or perform appropriate error handling
        }
      })


    setEmail('');
    setPassword('');

    console.log(`${email} has been verified! Welcome back!`);
  }


    useEffect(() => {
      if (email !== '' && password !== '') {
        setIsActive(true);
      } else {
        setIsActive(false);
      }
    }, [email, password]);

    return (
      <>
        {user.id !== null ? (
          <Navigate to="/products" />
        ) : (
          <div className="login-page" /*style={{ height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}*/>
            <div className="login-form">
              <Form onSubmit={(e) => authenticate(e)} className="loginForm">
                <h2 style={{ textAlign: 'center' }}>Login</h2>


                <Form.Group className="mb-3" controlId="userEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="name@example.com"
                    size="sm"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="userPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="password"
                    size="sm"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </Form.Group>

                {isActive ? (
                  <Button
                    className="loginBtn"
                    variant="custom"
                    style={{ backgroundColor: '#57675c', borderColor: '#57675c', color: 'white' }}
                    type="submit"
                    id="submitBtn"
                  >
                    Login
                  </Button>
                ) : (
                  <Button
                    className="loginBtn"
                    variant="custom"
                    style={{ backgroundColor: '#610a10', borderColor: '#610a10', color: 'white' }}
                    type="submit"
                    id="submitBtn"
                  >
                    Login
                  </Button>
                )}
              </Form>
            </div>
          </div>
        )}
      </>
    );
  }
