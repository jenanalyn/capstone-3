import React from 'react';

// holds/stores info from the global state
const UserContext = React.createContext();
// provider of global state to the components
export const UserProvider = UserContext.Provider;

export default UserContext;