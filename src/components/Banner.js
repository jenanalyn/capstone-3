import React from 'react';
import { Carousel, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({ data }) {
  const { title, content, destination, label } = data;

  const buttonStyle = {
    backgroundColor: '#610a10',
    color: 'white',
    // Add more styles as needed
  };

  return (
    <Carousel>
      <Carousel.Item>
        <div className="banner">
          <Row>
            <Col sm={12} md={{ span: 8, offset: 2 }} lg={{ span: 6, offset: 3 }} className="text-center">
              <h1 className="shopee-spy-title">{title}</h1>
              <p>{content}</p>
              <Button as={Link} to={destination} variant="custom" style={buttonStyle}>
                {label}
              </Button>
            </Col>
          </Row>
        </div>
      </Carousel.Item>
      <Carousel.Item>
        <div className="banner">
          <Row>
            <Col sm={12} md={{ span: 8, offset: 2 }} lg={{ span: 6, offset: 3 }} className="text-center">
              <h1 className="shopee-spy-title">{title}</h1>
              <p>Dare to experience the extraordinary and create unforgettable moments.</p>
              <Button as={Link} to={destination} variant="custom" style={buttonStyle}>
                {label}
              </Button>
            </Col>
          </Row>
        </div>
      </Carousel.Item>
      <Carousel.Item>
        <div className="banner">
          <Row>
            <Col sm={12} md={{ span: 8, offset: 2 }} lg={{ span: 6, offset: 3 }} className="text-center">
              <h1 className="shopee-spy-title">{title}</h1>
              <p>Discover love, laughter, and adventure with our spy toys and merchandise.</p>
              <Button as={Link} to={destination} variant="custom" style={buttonStyle}>
                {label}
              </Button>
            </Col>
          </Row>
        </div>
      </Carousel.Item>
    </Carousel>
  );
}
