import React, { useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  const isLoggedIn = user.id !== null;



  return (
    <Navbar bg="dark" variant="dark" expand="lg">
      <Container fluid>
        <Navbar.Brand as={Link} to="/">Shopee x Spy</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
            {
              (user.isAdmin)
              ?
              <Nav.Link as={NavLink} to="/admin">Admin Dashboard</Nav.Link>
              :
              <>
              <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
              <Nav.Link href="#cart">Cart</Nav.Link>
              </>
            }
            
            {
              (user.id !== null)
              ?
               <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
              :
              <NavDropdown title="Profile" id="basic-nav-dropdown">
                <NavDropdown.Item as={NavLink} to="/login">
                  Login
                </NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/registration">
                  Register
                </NavDropdown.Item>
              </NavDropdown>
            }            
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
