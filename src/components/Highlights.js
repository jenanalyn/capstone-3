import { Row, Col, Card } from 'react-bootstrap';
import spy from '../productImages/spy.gif'

export default function Highlights() {
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<div className="spy-image-container">
					  <img src={spy} alt="Loid" className="moving-image" />
					</div>
					    <Card.Body>
					        <Card.Title>Embrace Innovation</Card.Title>		        
					        <Card.Text>
					          Embrace innovation and unlock endless possibilities for growth and success.
					        </Card.Text>				       
					    </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<div className="spy-image-container">
					  <img src={spy} alt="Loid" className="moving-image" />
					</div>
				    <Card.Body>
				        <Card.Title>Elevate Your Style</Card.Title>		        
				        <Card.Text>
				          Dare to experience the extraordinary and create unforgettable moments.
				        </Card.Text>				       
				    </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<div className="spy-image-container">
					  <img src={spy} alt="Loid" className="moving-image" />
					</div>
				    <Card.Body>
				        <Card.Title>Ensemble Spy Cuties</Card.Title>		        
				        <Card.Text>
				         Discover love, laughter, and adventure with our spy toys and merchandise.
				        </Card.Text>				       
				    </Card.Body>
				</Card>
			</Col>

		</Row>
	)
}